/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MAX_RACHA;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TIPOS_CONSUMIDORES;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TIPOS_PRODUCTORES;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TOTAL_CONSUMIDORES;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TOTAL_PRODUCTORES;
import es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.aleatorio;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Practica1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        int[] elementos;
        Buffer buffer;
        Semaphore exmElm;
        Semaphore exmBuffer;
        Semaphore exmSelectorAB;
        Semaphore[][] sincProductor;
        Semaphore[] sincConsumidor;
        ArrayList<TipoElemento> selectorAB;
        ExecutorService ejecucion;
        ArrayList<Future<?>> listaTareas;
        Contador contadorAB;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos las variables del sistema
        buffer = new Buffer();
        listaTareas = new ArrayList();
        elementos = new int[TIPOS_PRODUCTORES];
        sincProductor = new Semaphore[TIPOS_PRODUCTORES][MAX_RACHA];
        for (int i = 0; i < TIPOS_PRODUCTORES; i++) {
            elementos[i] = 0;
            for (int j = 0; j < MAX_RACHA; j++) {
                sincProductor[i][j] = new Semaphore(0);
            }
        }
        sincConsumidor = new Semaphore[TIPOS_CONSUMIDORES];
        for (int i = 0; i < TIPOS_CONSUMIDORES; i++) {
            sincConsumidor[i] = new Semaphore(0);
        }
        exmElm = new Semaphore(1);
        exmBuffer = new Semaphore(1);
        exmSelectorAB = new Semaphore(1);
        selectorAB = new ArrayList<>();
        contadorAB = new Contador(0);

        // Creamos las diferentes tareas y las ejecutamos
        ejecucion = Executors.newCachedThreadPool();

        for (int i = 0; i < TOTAL_CONSUMIDORES; i++) {
            TipoElemento tipoElemento = TipoElemento.values()[aleatorio.nextInt(TIPOS_CONSUMIDORES)];
            Consumidor consumidor = new Consumidor("Consumidor-" + tipoElemento, tipoElemento,
                    elementos, buffer, exmElm, exmBuffer, exmSelectorAB,
                    sincProductor, sincConsumidor, selectorAB, contadorAB);
            Future<?> tarea = ejecucion.submit(consumidor);
            listaTareas.add(tarea);
        }

        for (int i = 0; i < TOTAL_PRODUCTORES; i++) {
            TipoElemento tipoElemento = TipoElemento.values()[aleatorio.nextInt(TIPOS_PRODUCTORES)];
            Productor productor = new Productor("Productor(" + i + "-" + tipoElemento + ")", tipoElemento,
                    elementos, buffer, exmElm, exmBuffer, exmSelectorAB, sincProductor, sincConsumidor, selectorAB, contadorAB);
            Future<?> tarea = ejecucion.submit(productor);
            listaTareas.add(tarea);
        }

        // Realizamos la ejecución por un minuto antes de pedir su cancelación
        System.out.println("(HILO_PRINCIPAL) Espera por un minuto");
        TimeUnit.MINUTES.sleep(TIEMPO_ESPERA);

        // Cancelamos las tareas y esperamos a su finalizacion
        for (Future<?> tarea : listaTareas) {
            tarea.cancel(true);
        }

        ejecucion.shutdown();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);

        System.out.println("(HILO_PRINCIPAL) Estado final del Buffer\n\t" + buffer);

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}
