/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.solucionpractica1;

import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MAX_RACHA;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MIN_RACHA;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TIEMPO_PRODUCCION;
import es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.TipoElemento.AoB;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.aleatorio;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.solucionpractica1.Contantes.MAX_ELEMENTOS;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class Productor implements Runnable {
    private final String iD;
    private final TipoElemento tipoElemento;
    private final int[] elementos;
    private final Buffer buffer;
    private final Semaphore exmElm;
    private final Semaphore exmBuffer;
    private final Semaphore exmSelectorAB;
    private final Semaphore[][] sincProductor;
    private final Semaphore[] sincConsumidor;
    private final ArrayList<TipoElemento> selectorAB;
    private final Contador contadorAB;

    public Productor(String iD, TipoElemento tipoElemento, int[] elementos, Buffer buffer, 
                     Semaphore exmElm, Semaphore exmBuffer, Semaphore exmSelectorAB, 
                     Semaphore[][] sincProductor, Semaphore[] sincConsumidor, 
                     ArrayList<TipoElemento> selectorAB, Contador contadorAB) {
        this.iD = iD;
        this.tipoElemento = tipoElemento;
        this.elementos = elementos;
        this.buffer = buffer;
        this.exmElm = exmElm;
        this.exmBuffer = exmBuffer;
        this.exmSelectorAB = exmSelectorAB;
        this.sincProductor = sincProductor;
        this.sincConsumidor = sincConsumidor;
        this.selectorAB = selectorAB;
        this.contadorAB = contadorAB;
        
    }
    
    @Override
    public void run() {
        System.out.println("TAREA(" + iD + ") ha comenzado su ejecución");
        
        try {
            int racha = aleatorio.nextInt(MAX_RACHA) + MIN_RACHA;
            comprobarEspacio(racha);
            almacenarElementos(racha);
            liberarProcesos();
            
            System.out.println("TAREA(" + iD + ") ha finalizado su ejecución");
        } catch( InterruptedException ex ) {
            System.out.println("TAREA(" + iD + ") CANCELADO");
        } 
    }

    private void comprobarEspacio(int racha) throws InterruptedException {
        exmElm.acquire();
        if( elementos[tipoElemento.ordinal()] + racha <= MAX_ELEMENTOS[tipoElemento.ordinal()] ) {
            elementos[tipoElemento.ordinal()] += racha;
            exmElm.release();
        } else {
            exmElm.release();
            sincProductor[tipoElemento.ordinal()][racha-1].acquire();
        }
    }
    
    private void almacenarElementos(int racha) throws InterruptedException {
        exmBuffer.acquire();
        for(int i = 1; i <= racha; i++) {
            if( i != racha  )
                buffer.add(new Elemento(tipoElemento));
            else 
                buffer.add(new Elemento(tipoElemento,true));
        }
        exmBuffer.release();
        
        System.out.println("TAREA(" + iD + ") ha almacenado " + racha + " elementos de tipo " +
                           tipoElemento );
        TimeUnit.SECONDS.sleep(TIEMPO_PRODUCCION);
    }
    
    private void liberarProcesos() throws InterruptedException {
        boolean liberado;
        int tamRacha;
        
        // Liberamos un consumidor AoB si hay esperando y le asignamos un valor a consumir
        exmSelectorAB.acquire();
        if( contadorAB.getContador() > 0 ) {
            //Hay que indicar que tipo debe de usar al consumidor AB
            selectorAB.add(tipoElemento);
            contadorAB.restar();
            sincConsumidor[AoB.ordinal()].release();
        } else {
            // Si no uno de los del tipoElemento
            sincConsumidor[tipoElemento.ordinal()].release();
        }
        exmSelectorAB.release();
        
        // Comprobamos si se puede liberar algún productor porque hay
        // espacio en el Buffer, empezando con el que consuma más espacio del Buffer
        exmElm.acquire();
        liberado = false;
        tamRacha = MAX_RACHA;
        
        while( (tamRacha >= MIN_RACHA) & !liberado ) {
            if( (elementos[tipoElemento.ordinal()] + tamRacha <= MAX_ELEMENTOS[tipoElemento.ordinal()]) &&
                sincProductor[tipoElemento.ordinal()][tamRacha-1].hasQueuedThreads() ) {
                
                elementos[tipoElemento.ordinal()] += tamRacha;
                liberado = true;
                sincProductor[tipoElemento.ordinal()][tamRacha-1].release();
            }
                
            tamRacha--;
        }
        exmElm.release();
    }
}
